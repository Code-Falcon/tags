/**
 * Code by CaptainDawg
 */
package com.captaindawg.tags;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

public class TagCore extends JavaPlugin implements Listener {

    @Override
    public void onEnable(){
        getServer().getPluginManager().registerEvents(this, this);
    }

    public static void setTag(Player player, ChatColor colour){
        Scoreboard board = Bukkit.getScoreboardManager().getMainScoreboard();
        Team color = board.getTeam(player.getName());
        if (color == null) {
            color = board.registerNewTeam(player.getName());
        }
        color.setPrefix(colour + "");
        color.addPlayer(player);

        player.setScoreboard(board);
    }


    public static void setTag(Player player, ChatColor colour, String prefix){
        Scoreboard board = Bukkit.getScoreboardManager().getMainScoreboard();
        Team color = board.getTeam(player.getName());
        if (color == null) {
            color = board.registerNewTeam(player.getName());
        }
        color.setPrefix(colour + prefix);
        color.addPlayer(player);

        player.setScoreboard(board);
    }

    public static void clearTag(Player player){
        Bukkit.getScoreboardManager().getMainScoreboard().getTeam(player.getName()).unregister();
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e){
        clearTag(e.getPlayer());
    }
}
